import { Component } from '@angular/core';
import { NavController,ActionSheetController } from 'ionic-angular';
import {Http, Response,RequestOptions,Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { GooglePlus } from '@ionic-native/google-plus';
import { CallNumber } from '@ionic-native/call-number';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { SocialSharing } from '@ionic-native/social-sharing';
import { SessionService } from '../../app/sessionservice';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  mobileNo:any;
  otp:any;
  headers:any;
  capturedImage:any;
  base64Image:any;
  constructor(public navCtrl: NavController,public http:Http,public fb: Facebook,public googlePlus: GooglePlus,public callNumber: CallNumber,public camera: Camera,public actionCtrl:ActionSheetController,public socialSharing: SocialSharing,public service:SessionService) {
    this.mobileNo="997167288122";
    this.headers = new Headers({'Content-Type':'application/json'});
  }


  sendOtp()
  {
    // alert("clicked");

    
    // headers1.append('Accept', 'application/json');
    // headers.append('Content-Type', 'application/json');
    // headers.append('Access-Control-Allow-Origin', '*');
    // headers.append('Access-Control-Allow-Credentials', 'true');
    // headers.append("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE");
    // headers.append("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token");

    var mobile="91"+this.mobileNo;
    // let options = new RequestOptions({ headers: headers });
        // let options = new RequestOptions({url:'GET',headers:headers1});
    // let options = new RequestOptions({ headers: headers });
      
    var smsUrl="https://control.msg91.com/api/sendotp.php?authkey=169096A9g9vil6eKqv598ab8f0&mobile="+mobile+"&otp_expiry=2"; 
    
     this.http.get(smsUrl,{headers: this.headers})
      .map(val => val.json())
      .subscribe(data => 
        {
          alert("Success::"+JSON.stringify(data));
          console.log(JSON.stringify(data))
        })

      err =>
				{
				 alert("Error"+err);
        }     
  }


  verify()
  {
    // alert("caled");
    if(this.otp.length==4)
      {
      // alert("Otp length match");  
      var mobile="91"+this.mobileNo;  
      var verifyUrl="https://control.msg91.com/api/verifyRequestOTP.php?authkey=169096A9g9vil6eKqv598ab8f0&mobile="+mobile+"&otp="+this.otp;  
      this.http.get(verifyUrl,{headers: this.headers})
      .map(val => val.json())
      .subscribe(data => 
        {
          alert("Success::"+JSON.stringify(data));
          console.log(JSON.stringify(data))
        })

      err =>
				{
				 alert("Error"+err);
        }     
      }
  }
  
  resend()
  {
      alert("send");
      var mobile="91"+this.mobileNo;       
      var verifyUrl="https://control.msg91.com/api/retryotp.php?authkey=169096A9g9vil6eKqv598ab8f0&mobile="+mobile;
      this.http.get(verifyUrl,{headers: this.headers})
      .map(val => val.json())
      .subscribe(data => 
        {
          alert("Success::"+JSON.stringify(data));
          console.log(JSON.stringify(data))
        })

      err =>
				{
				 alert("Error"+err);
        }     
      

  } 

  fbLogin()
  {
    this.fb.login(['email', 'public_profile'])
    .then((res: FacebookLoginResponse) =>{
      this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile =>{
        var data= {email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name']}
        alert('Logged into Facebook!'+JSON.stringify(data));
      })
    })  
    .catch(e =>{
      alert("Error Logged in"+e);
    }) 
  }

  googleLogin()
  {
    this.googlePlus.login({})
    .then(res =>{
      alert("Success::--"+res);
      alert("google data::"+JSON.stringify(res));
    }) 
    .catch(err =>{
      alert("Error::--"+err);
      alert("google data errr::"+JSON.stringify(err));
      }) 
  }

  call()
  {
    //  alert("Calling");
    this.callNumber.callNumber("8510070977", true)
    .then(() =>{
      // alert("Launching dialer");
    }) 
    .catch(() =>{
      alert("Error While Launching Dialer");
    }) 
  }
  takePictureWithType(type)
  {
    const options: CameraOptions = {
        quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        sourceType: type,
    }

    this.camera.getPicture(options).then((imageData) => {
    // imageData is either a base64 encoded string or a file URI
    // If it's base64:

    this.capturedImage=imageData;
    this.base64Image = 'data:image/jpeg;base64,' + imageData;

    // alert("image::-"+base64Image);
    }, (err) => {
    // Handle error
    });
  }

  shareApp()
  {
    this.socialSharing.share().then(() => {
      // Sharing via email is possible
    }).catch(() => {
      // Sharing via email is not possible
    });

  }
  takePicture()
  { 
    // alert("Calling");

    let actionSheet = this.actionCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePictureWithType(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePictureWithType(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }


  sendPostData()
  {
    alert("New Post-------------");
      var body={
        "title":"Hello Post api"
      };
      let options = new RequestOptions
      ({
     
        headers:this.headers,
        body:body
        });
      
     var postUrl="http://autorepair-testapi.onlinewebshop.net/wp-json/wp/v2/posts";     
     this.http.post(postUrl,options)
      .map(val => val.json())
      .subscribe(data => 
        {
          alert("Success::"+JSON.stringify(data));
          console.log(JSON.stringify(data))
        })

      err =>
				{
				 alert("Error"+err);
        }     
    

  } 



  sendNotification()
  {
     var notifyUrl="http://klaspring.staging.wpengine.com/push_api.php?token="+this.service.getToken();     
     this.http.get(notifyUrl,{headers: this.headers})
      // .map(val => val.json())
      .subscribe(data => 
        {
          alert("Success::"+JSON.stringify(data));
          console.log(JSON.stringify(data))
        })
        err =>
				{
				 alert("Error"+err);
        }     
  }
  
    
}
