import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { FCM } from '@ionic-native/fcm';
import { SessionService } from './sessionservice';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,public fcm:FCM,public service:SessionService) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.initPushNotification();

    });
  }
   initPushNotification()
    {

      this.fcm.subscribeToTopic('Notification');

      this.fcm.getToken().then(token=>{
        alert("token=="+token);  
        console.log("token=="+JSON.stringify(token));

        this.service.setToken(token);
      }).catch( (e) => {
          alert("error"+e);
          // //alert(JSON.stringify(e));
          });

      this.fcm.onNotification().subscribe(data=>{
        if(data.wasTapped){
            alert("recieved notification=="+JSON.stringify(data));
        } else {
           alert("received notification without tap=="+JSON.stringify(data))
        };
      })

      this.fcm.onTokenRefresh().subscribe(token=>{
         
         console.log("refresh token==="+JSON.stringify(token));
         this.service.setToken(token);
      })

      this.fcm.unsubscribeFromTopic('Notification');
    }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
